package org.example;

public class Account {
    private final String user;
    private final String pass;
    private final String greetingsMsg;

    public Account(String user, String pass) {
        this.user = user;
        this.pass = pass;
        this.greetingsMsg = "Hi " + user + "!";
    }

    public String getUser() {
        return user;
    }

    public String getPass() {
        return pass;
    }

    public String getGreetingsMsg() {
        return greetingsMsg;
    }

    @Override
    public String toString() {
        return user + "/" + pass;
    }
}
