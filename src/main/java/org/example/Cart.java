package org.example;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class Cart {

    private final SelenideElement cartPageTitle = $("h1 .text-muted");

    public String getCartPageTitle() {
        return cartPageTitle.text();
    }
}
