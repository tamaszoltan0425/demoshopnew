package org.example;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.commands.PressEnter;
import dataprovider.AuthenticationDataProvider;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import static org.testng.Assert.*;

public class LoginTest {
    LoginModal loginModal = new LoginModal();
    Homepage homepage = new Homepage();
    Header header = new Header();


    @BeforeMethod
    public void setup() {
        homepage.openHomepage();
    }

    @AfterMethod
    public void cleanup() {
        Selenide.refresh();
        Selenide.closeWebDriver();
    }

    @Test(dataProvider = "validCredentials", dataProviderClass = AuthenticationDataProvider.class)
    public void valid_user_using_valid_credentials_can_successfully_login_by_clicking_login_button(Account account) {
        header.clickLogin();
        loginModal.typeInUsername(account.getUser());
        loginModal.typeInPassword(account.getPass());
        loginModal.clickToLogin();
        assertEquals(header.getGreetingMessage(), "Hi " + account.getUser() + "!");
    }

    @Test(dataProvider = "validCredentials", dataProviderClass = AuthenticationDataProvider.class)
    public void valid_user_using_valid_credentials_can_successfully_login_by_hitting_enter_key(Account account) {
        header.clickLogin();
        loginModal.typeInUsername(account.getUser());
        loginModal.typeInPassword(account.getPass());
        loginModal.hitEnterToLogin();
        assertEquals(header.getGreetingMessage(), "Hi " + account.getUser() + "!");
    }
}
