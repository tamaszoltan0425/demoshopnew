package org.example;

import com.codeborne.selenide.SelenideElement;

import java.util.List;

import static com.codeborne.selenide.Selenide.$;

public class ProductsListing {
    private final SelenideElement searchInput = $(".form-inline #input-search");
    private final SelenideElement searchButton = $(".form-inline button");

    public void inputSearchQuery(String searchWord) {
        searchInput.type(searchWord);
    }

    public void clickSearchButton() {
        this.searchButton.click();
    }





}
