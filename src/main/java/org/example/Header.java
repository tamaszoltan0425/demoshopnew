package org.example;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class Header {
    private final SelenideElement shoppingBagLogo = $(".fa-shopping-bag");
    private final SelenideElement miniCartIcon = $(".fa-shopping-cart");
    private final SelenideElement wishListIcon = $(".navbar-nav .fa-heart");
    private final SelenideElement headerGreetingText = $(".navbar-text span span");
    private final SelenideElement signInButton = $(".fa-sign-in-alt");

    public void clickToGoOnHomepage() {
        this.shoppingBagLogo.click();
    }

    public void clickMiniCartIcon() {
        this.miniCartIcon.click();
    }

    public void clickWishlistIcon() {
        this.wishListIcon.click();
    }

    public String getGreetingMessage() {
        return this.headerGreetingText.text();
    }

    public void clickLogin() {
        this.signInButton.click();
    }


}
