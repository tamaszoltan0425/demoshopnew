package org.example;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class Wishlist {
    private final SelenideElement wishlistPageTitle = $("h1 .text-muted");

    public String getWishlistPageTitle() {
        return wishlistPageTitle.text();
    }
}
