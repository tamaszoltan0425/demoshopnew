package dataprovider;
import org.example.Account;
import org.testng.annotations.DataProvider;

public class AuthenticationDataProvider {

    @DataProvider(name = "validCredentials")
    public Object[][] getCredentials() {
        Account dino = new Account("dino", "choochoo");
        Account beetle = new Account("beetle", "choochoo");
        Account ducker = new Account("ducker", "choochoo");
        Account turtle = new Account("turtle", "choochoo");
        return new Object[][]{
                {dino},
                {beetle},
                {ducker},
                {turtle}
        };
    }


}
