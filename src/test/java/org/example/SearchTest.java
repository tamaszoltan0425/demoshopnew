package org.example;

import com.codeborne.selenide.Selenide;
import dataprovider.SearchQueryDataProvider;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class SearchTest {

    Homepage homepage = new Homepage();
    ProductsListing productsListing = new ProductsListing();
    SearchResults searchResults = new SearchResults();

    @BeforeMethod
    public void setup() {homepage.openHomepage();
    }

    @AfterMethod
    public void cleanup() {
        Selenide.refresh();
        Selenide.closeWebDriver();
    }

    @Test(dataProvider = "inValidSearchQuery", dataProviderClass = SearchQueryDataProvider.class)
    public void using_invalid_search_query_will_not_return_products(SearchQuery searchQuery) {
        productsListing.inputSearchQuery(searchQuery.getSearchItem());
        productsListing.clickSearchButton();
        assertTrue(searchResults.getResultTitles().isEmpty(), "Products searched for are non existent");
    }

    @Test(dataProvider = "validSearchQuery", dataProviderClass = SearchQueryDataProvider.class)
    public void using_valid_search_query_returns_products_searched_for(SearchQuery searchQuery) {
        productsListing.inputSearchQuery(searchQuery.getSearchItem());
        productsListing.clickSearchButton();
        assertTrue(searchResults.isSearchWordContainedInTitles(searchQuery.getSearchItem()), "Query is valid.");

    }
}
