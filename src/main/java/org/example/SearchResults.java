package org.example;

import com.codeborne.selenide.ElementsCollection;

import java.util.List;

import static com.codeborne.selenide.Selenide.$$;

public class SearchResults {
    private final ElementsCollection resultTitles = $$(".card-link");

    public List<String> getResultTitles() {
        return resultTitles.texts();
    }

    public boolean isSearchWordContainedInTitles(String searchWord) {
        boolean contains = false;
        List<String> texts = resultTitles.texts();
        for (String text : texts) {
            System.out.println(text);
            text = text.toLowerCase();
            contains = text.contains(searchWord);
        }
        return contains;

    }
}
