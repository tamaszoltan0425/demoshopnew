package org.example;

public class SearchQuery {

    private final String searchItem;

    public SearchQuery(String searchItem) {
        this.searchItem = searchItem;
    }

    public String getSearchItem() {
        return searchItem.toLowerCase();
    }

}
