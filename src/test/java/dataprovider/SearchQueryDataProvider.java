package dataprovider;
import org.example.SearchQuery;
import org.testng.annotations.DataProvider;

public class SearchQueryDataProvider {
    @DataProvider(name = "validSearchQuery")
    public Object[][] getValidSearchQuery() {
        SearchQuery awesome = new SearchQuery("awesome");
        SearchQuery pizza = new SearchQuery("pizza");
        SearchQuery practical = new SearchQuery("practical");
        SearchQuery halfWord = new SearchQuery("pract");
        SearchQuery latin = new SearchQuery("lectus");
        return new Object[][] {
                {awesome},
                {pizza},
                {practical},
                {halfWord},
                {latin}
        };
    }
    @DataProvider(name = "inValidSearchQuery")
    public Object[][] getInvalidSearchQuery() {
        SearchQuery number = new SearchQuery("123");
        SearchQuery special = new SearchQuery("!@#");
        SearchQuery empty = new SearchQuery("");
        return new Object[][] {
                {number},
                {special},
                {empty}
        };
    }

}
