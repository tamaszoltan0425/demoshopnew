package org.example;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class LoginModal {
    private final SelenideElement loginModalTitle = $(".modal-title");
    private final SelenideElement usernameInputField = $("form #user-name");
    private final SelenideElement passwordInputField = $("form #password");
    private final SelenideElement loginButton = $("form button .fa-sign-in-alt");
    private final SelenideElement errorMessage = $(".error");
    private final SelenideElement closeModalButton = $(".close");

    public boolean isModalDisplayed() {
        return loginModalTitle.isDisplayed();
    }

    public void typeInUsername(String user) {
        this.usernameInputField.click();
        usernameInputField.type(user);
    }

    public void typeInPassword(String pass) {
        this.passwordInputField.click();
        passwordInputField.type(pass);
    }
    public void clickToLogin() {
        this.loginButton.click();
    }
    public void hitEnterToLogin() {
        this.passwordInputField.pressEnter();
    }
}
