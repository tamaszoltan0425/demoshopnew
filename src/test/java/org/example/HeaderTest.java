package org.example;

import com.codeborne.selenide.Selenide;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.sleep;
import static org.testng.Assert.*;

public class HeaderTest {

    Header header = new Header();
    Homepage homepage = new Homepage();
    Cart cart = new Cart();
    Wishlist wishlist = new Wishlist();
    LoginModal loginModal = new LoginModal();

    @BeforeMethod
    public void setup() {
        homepage.openHomepage();
    }

    @AfterMethod
    public void cleanup() {
        Selenide.refresh();
        Selenide.closeWebDriver();
    }

    @Test
    public void user_can_navigate_to_cartpage_by_clicking_minicart_icon() {
        header.clickMiniCartIcon();
        assertEquals(cart.getCartPageTitle(), "Your cart", "User should be on Cart page");
    }
    @Test
    public void user_can_navigate_to_homepage_by_clicking_shopping_bag_icon() {
        header.clickMiniCartIcon();
        header.clickToGoOnHomepage();
        assertEquals(homepage.getHomePageTitle(), "Products", "User should be on Homepage");
    }
    @Test
    public void user_can_navigate_to_wishlist_by_clicking_heart_icon() {
        header.clickWishlistIcon();
        assertEquals(wishlist.getWishlistPageTitle(), "Wishlist", "User should be on Wishlist page");
    }
    @Test
    public void guest_user_is_greeted_by_hello_guest_message() {
        assertEquals(header.getGreetingMessage(), "Hello guest!", "Not logged in user is greeted by 'Hello Guest' message");
    }

    @Test
    public void clicking_the_login_button_prompts_a_login_modal() {
        header.clickLogin();
        assertTrue(loginModal.isModalDisplayed());
    }

}