package org.example;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

public class Homepage {
    public static final String URL = "https://fasttrackit-int.netlify.app/#/";
    private final SelenideElement homePageTitle = $("h1 .text-muted");

    public void openHomepage() {
        open(URL);
    }

    public String getHomePageTitle() {
        return homePageTitle.text();
    }
}
