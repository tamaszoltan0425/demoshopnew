package org.example;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class Product {
    private final SelenideElement card;
    private final String title;
    private final SelenideElement addToCartButton;
    private final String price;

    public Product(String productId) {
        String productIDSelector = String.format("[href='#/product/%s']", productId);
        SelenideElement cardLink = $(productIDSelector);
        this.card = cardLink.parent().parent();
        this.addToCartButton =card.$(".card-footer .fa-cart-plus");
        this.title = cardLink.text();
        this.price = this.card.$(".card-footer .card-text span").text();

    }
    public String getTitle() {

        return title;
    }
    public String getPrice() {
        return price;
    }


    public void clickOnProductCartIcon() {
        this.addToCartButton.click();
    }
}
